# ExeServer

This project is an example of simple exe server.

## Server

Server provides two interfaces, ICalc and ISciCalc.
It also provides a UI, which uses these interfaces.

## Client

Client is using exe server, to obtain the ICalc and ISciCalc objects.
It is using these interfaces in a console based application.

Test comments for branch test1.
