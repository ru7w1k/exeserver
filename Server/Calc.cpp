/*
 * Implementation of CoClass Calc, implementing IStdCalc and ISciCalc 
 */

#include <Windows.h>

#include "StdCalc.h"
#include "SciCalc.h"
#include "Calc.h"


//// Helper Functions ////

double ToRadian(double dAngle)
{
    return (dAngle * (::PI() / 180.0));
}

//////////////////////////

// CoClass CCalc
class CCalc : public IStdCalc, ISciCalc
{
private:
    long m_cRef;
    int  m_iUnitOfAngle;

public:

    CCalc(void);
    ~CCalc(void);

    // IUnknown Interface
    HRESULT __stdcall QueryInterface  (REFIID, void **);
    ULONG   __stdcall AddRef          (void);
    ULONG   __stdcall Release         (void);


    // IStdCalc Interface
    HRESULT __stdcall Addition        (double, double, double *);
    HRESULT __stdcall Subtraction     (double, double, double *);
    HRESULT __stdcall Multiplication  (double, double, double *);
    HRESULT __stdcall Division        (double, double, double *);
    HRESULT __stdcall Square          (double, double *);
    HRESULT __stdcall SquareRoot      (double, double *);

    // ISciCalc Interface
    HRESULT __stdcall Sin         (double, double *);        
    HRESULT __stdcall Cos         (double, double *);        
    HRESULT __stdcall Tan         (double, double *);        
    HRESULT __stdcall SinInverse  (double, double *);        
    HRESULT __stdcall CosInverse  (double, double *);        
    HRESULT __stdcall TanInverse  (double, double *);        
    HRESULT __stdcall Log         (double, double *);        
    HRESULT __stdcall Ln          (double, double *);        
    HRESULT __stdcall Factorial   (double, double *);        
    HRESULT __stdcall PI          (double *);                
    HRESULT __stdcall XToPowerY   (double, double, double *);
};

// Class Factory 
class CCalcClassFactory : public IClassFactory
{
private:
    long m_cRef;

public:

    CCalcClassFactory(void);
    ~CCalcClassFactory(void);

    // IUnknown Interface
    HRESULT __stdcall QueryInterface  (REFIID, void **);
    ULONG   __stdcall AddRef          (void);
    ULONG   __stdcall Release         (void);

    // IClassFactory Interface
    HRESULT __stdcall CreateInstance (IUnknown *, REFIID, void **);
    HRESULT __stdcall LockServer     (BOOL);

};


// global variable declaration
long glNumberOfActiveComponents = 0;
long glNumberOfServerLocks = 0;
IClassFactory *gpIClassFactory = NULL;
HWND gHwnd = NULL;
DWORD dwRegister;

// Implementation of CCalc's methods
CCalc::CCalc(void)
{
	m_cRef = 1;
    m_iUnitOfAngle = DEGREE;
	InterlockedIncrement(&glNumberOfActiveComponents);
}

CCalc::~CCalc(void)
{
	InterlockedDecrement(&glNumberOfActiveComponents);
}

// Implementation Of CCalc's IUnknown Interface
HRESULT CCalc::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IStdCalc *>(this);
	else if (riid == IID_IStdCalc)
		*ppv = static_cast<IStdCalc*>(this);
	else if (riid == IID_ISciCalc)
		*ppv = static_cast<ISciCalc *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();
	return(S_OK);
}

ULONG CCalc::AddRef(void)
{
	InterlockedIncrement(&glNumberOfActiveComponents);
	return(m_cRef);
}

ULONG CCalc::Release(void)
{
	InterlockedDecrement(&glNumberOfActiveComponents);

	if (m_cRef == 0)
	{
		delete(this);

		if (glNumberOfActiveComponents == 0 && glNumberOfServerLocks == 0)
			PostMessage(gHwnd, WM_QUIT, (WPARAM)0, (LPARAM)0L);

		return(0);
	}
	return(m_cRef);
}

// Implementation Of CCalc's IStdCalc Interface
HRESULT CCalc::Addition(double dNum1, double dNum2, double *pdResult)
{
    *pdResult = _Addition(dNum1, dNum2);
    return S_OK;
}

HRESULT CCalc::Subtraction(double dNum1, double dNum2, double *pdResult)
{
    *pdResult = _Subtraction(dNum1, dNum2);
    return S_OK;
}

HRESULT CCalc::Multiplication(double dNum1, double dNum2, double *pdResult)
{
    *pdResult = _Multiplication(dNum1, dNum2);
    return S_OK;
}

HRESULT CCalc::Division(double dNum1, double dNum2, double *pdResult)
{
    if (dNum2 == 0)
    {
        *pdResult = NULL;
        return S_FALSE;
    }
    *pdResult = _Division(dNum1, dNum2);
    return S_OK;
}

HRESULT CCalc::Square(double dNum1, double *pdResult)
{
    *pdResult = _Square(dNum1);
    return S_OK;
}

HRESULT CCalc::SquareRoot(double dNum1, double *pdResult)
{
    if (dNum1 < 0)
    {
        *pdResult = NULL;
        return S_FALSE;
    }

    *pdResult = _SquareRoot(dNum1);
    return S_OK;
}


// Implementation Of CCalc's ISciCalc Interface
HRESULT CCalc::Sin(double dAngle, double *pdResult)
{
    if (m_iUnitOfAngle == DEGREE)
        dAngle = ToRadian(dAngle);

    *pdResult = _Sin(dAngle);
    return(S_OK);
}

HRESULT CCalc::Cos(double dAngle, double *pdResult)
{
    if (m_iUnitOfAngle == DEGREE)
        dAngle = ToRadian(dAngle);

    *pdResult = _Cos(dAngle);
    return(S_OK);
}

HRESULT CCalc::Tan(double dAngle, double *pdResult)
{
    if (m_iUnitOfAngle == DEGREE)
        dAngle = ToRadian(dAngle);

    *pdResult = _Tan(dAngle);
    return(S_OK);
}

HRESULT CCalc::SinInverse(double dAngle, double *pdResult)
{
    if (m_iUnitOfAngle == DEGREE)
        dAngle = ToRadian(dAngle);

    *pdResult = _SinInverse(dAngle);
    return(S_OK);
}

HRESULT CCalc::CosInverse(double dAngle, double *pdResult)
{
    if (m_iUnitOfAngle == DEGREE)
        dAngle = ToRadian(dAngle);

    *pdResult = _CosInverse(dAngle);
    return(S_OK);
}

HRESULT CCalc::TanInverse(double dAngle, double *pdResult)
{
    if (m_iUnitOfAngle == DEGREE)
        dAngle = ToRadian(dAngle);

    *pdResult = _TanInverse(dAngle);
    return(S_OK);
}

HRESULT CCalc::Log(double dNum, double *pdResult)
{
    *pdResult = _Log(dNum);
    return(S_OK);
}

HRESULT CCalc::Ln(double dNum, double *pdResult)
{
    *pdResult = _Ln(dNum);
    return(S_OK);
}

HRESULT CCalc::Factorial(double dNum, double *pdResult)
{
    *pdResult = (double) _Factorial((int)dNum);
    return(S_OK);
}

HRESULT CCalc::PI(double *pdResult)
{
    *pdResult = ::PI();
    return(S_OK);
}

HRESULT CCalc::XToPowerY(double dX, double dY, double *pdResult)
{
    *pdResult = _XToPowerY(dX, dY);
    return(S_OK);
}


// Implementation Of CCalc's methods
CCalcClassFactory::CCalcClassFactory(void)
{
	m_cRef = 1;
}

CCalcClassFactory::~CCalcClassFactory(void)
{

}

// Implementation Of CCalc's IUnknown Interface
HRESULT CCalcClassFactory::QueryInterface(REFIID riid, void **ppv)
{
	if (riid == IID_IUnknown)
		*ppv = static_cast<IClassFactory *>(this);
	else if (riid == IID_IClassFactory)
		*ppv = static_cast<IClassFactory *>(this);
	else
	{
		*ppv = NULL;
		return(E_NOINTERFACE);
	}

	reinterpret_cast<IUnknown *>(*ppv)->AddRef();

	return(S_OK);
}

ULONG CCalcClassFactory::AddRef(void)
{
	InterlockedIncrement(&glNumberOfActiveComponents);
	return(m_cRef);
}

ULONG CCalcClassFactory::Release()
{
	InterlockedDecrement(&glNumberOfActiveComponents);

	if (m_cRef == 0)
	{
		delete(this);
		return(0);
	}

	return(m_cRef);
}

// Implementation Of CCalc's IClassFactory Intertface
HRESULT CCalcClassFactory::CreateInstance(IUnknown *pUnkOuter, REFIID riid, void **ppv)
{
	// variable declarations
	CCalc *pCalc = NULL;
	HRESULT hr;

	if (pUnkOuter != NULL)
		return(CLASS_E_NOAGGREGATION);

	// create the instance of component i.e. of CCalc class
	pCalc = new CCalc;
	if (pCalc == NULL)
		return(E_OUTOFMEMORY);

	// get the requested interface
	hr = pCalc->QueryInterface(riid, ppv);
	pCalc->Release();

	return(hr);
}

HRESULT CCalcClassFactory::LockServer(BOOL fLock)
{
	if (fLock)
		InterlockedIncrement(&glNumberOfServerLocks);
	else
		InterlockedDecrement(&glNumberOfServerLocks);
	return(S_OK);
}


//// EXE Server Methods //////////

HRESULT StartMyClassFactories(void)
{
	// variable declarations 
	HRESULT hr;

	gpIClassFactory = new CCalcClassFactory();
	if (gpIClassFactory == NULL)
		return(E_OUTOFMEMORY);

	gpIClassFactory->AddRef();

    //                                                                                       |       
	// register the class factory in COM's private database for SERVER!                      V
	hr = CoRegisterClassObject(CLSID_Calc, static_cast<IUnknown *>(gpIClassFactory), CLSCTX_INPROC_SERVER, REGCLS_MULTIPLEUSE, &dwRegister);
	if (FAILED(hr))
	{
		gpIClassFactory->Release();
		return(E_FAIL);
	}

    //                                                                                      |
	// register the class factory in COM's private database for CLIENT!                     V 
	hr = CoRegisterClassObject(CLSID_Calc, static_cast<IUnknown *>(gpIClassFactory), CLSCTX_LOCAL_SERVER, REGCLS_MULTIPLEUSE, &dwRegister);
	if (FAILED(hr))
	{
		gpIClassFactory->Release();
		return(E_FAIL);
	}

	return(S_OK);
}

void StopMyClassFactories(void)
{
	// un-register the class factory
	if (dwRegister != 0)
		CoRevokeClassObject(dwRegister);

	if (gpIClassFactory != NULL)
		gpIClassFactory->Release();
}

