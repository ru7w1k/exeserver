#pragma once

/////////   Constants   /////////////////////
#define DEGREE  1
#define RADIAN  2

/////////   Calculator Interfaces   /////////

// IStdCalc Interface
class IStdCalc : public IUnknown
{
public:
    virtual HRESULT __stdcall Addition        (double, double, double *)  = 0;
    virtual HRESULT __stdcall Subtraction     (double, double, double *)  = 0;
    virtual HRESULT __stdcall Multiplication  (double, double, double *)  = 0;
    virtual HRESULT __stdcall Division        (double, double, double *)  = 0;
    virtual HRESULT __stdcall Square          (double, double *)          = 0;
    virtual HRESULT __stdcall SquareRoot      (double, double *)          = 0;
};


// ISciCalc Interface
class ISciCalc : public IUnknown
{
public:
    virtual HRESULT __stdcall Sin         (double, double *)          = 0;
    virtual HRESULT __stdcall Cos         (double, double *)          = 0;
    virtual HRESULT __stdcall Tan         (double, double *)          = 0;
    virtual HRESULT __stdcall SinInverse  (double, double *)          = 0;
    virtual HRESULT __stdcall CosInverse  (double, double *)          = 0;
    virtual HRESULT __stdcall TanInverse  (double, double *)          = 0;
    virtual HRESULT __stdcall Log         (double, double *)          = 0;
    virtual HRESULT __stdcall Ln          (double, double *)          = 0;
    virtual HRESULT __stdcall Factorial   (double, double *)          = 0;
    virtual HRESULT __stdcall PI          (double *)                  = 0;
    virtual HRESULT __stdcall XToPowerY   (double, double, double *)  = 0;
};

// IID of IStdCalc {DC51D4CB-542D-4A95-BBE1-C2938FE85BA3}
const IID IID_IStdCalc = { 0xdc51d4cb, 0x542d, 0x4a95, 0xbb, 0xe1, 0xc2, 0x93, 0x8f, 0xe8, 0x5b, 0xa3 };

// IID of ISciCalc {6D78A2AB-F036-4D21-8078-38D99FB810C5}
const IID IID_ISciCalc = { 0x6d78a2ab, 0xf036, 0x4d21, 0x80, 0x78, 0x38, 0xd9, 0x9f, 0xb8, 0x10, 0xc5 };

// CLSID of CCalc {E268458B-B5A3-49B9-BA48-D61963E2B2DE}
const CLSID CLSID_Calc = { 0xe268458b, 0xb5a3, 0x49b9, 0xba, 0x48, 0xd6, 0x19, 0x63, 0xe2, 0xb2, 0xde };
