#pragma once

#include<math.h>							// for library functions
#define CALC_PI 3.141592653589


// core function definations
double _Sin(double dVar)
{
	return(sin(dVar));
}

double _Cos(double dVar)
{
	return(cos(dVar));
}

double _Tan(double dVar)
{
	return(tan(dVar));
}

double _SinInverse(double dVar)
{
	return(asin(dVar));
}

double _CosInverse(double dVar)
{
	return(acos(dVar));
}

double _TanInverse(double dVar)
{
	return(atan(dVar));
}

double _Log(double dVar)
{
	return(log10(dVar));
}


double _Ln(double dVar)
{
	return(log1p(dVar));
}

unsigned long _Factorial(int iVar)
{
	// declaration of local dVariable
	unsigned long ulFactorial = 1;

	// code
	if (iVar == 0)
		return(1);
	else if(iVar < 0)
		return(0);
	else
	{
		for (int i = 1; i <= iVar; i++)
			ulFactorial *= i;
		return(ulFactorial);
	}
}

double PI()
{
	return(CALC_PI);
}


double _XToPowerY(double dVar1, double dVar2)
{
	return(pow(dVar1, dVar2));
}
