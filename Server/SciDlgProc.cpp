// SciDlgProc.cpp
#define UNICODE

// Headers 
#include <Windows.h>
#include <strsafe.h>
#include <stdlib.h>

#include "Calc.h"
#include "Server.h"

BOOL CALLBACK SciDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	double dResult, dNum1, dNum2;
	static ISciCalc *pISciCalc = NULL;
	TCHAR str[255];
	HRESULT hr;

    switch (iMsg)
	{
        case WM_INITDIALOG:
            hr = CoCreateInstance(CLSID_Calc, NULL, CLSCTX_INPROC_SERVER, IID_ISciCalc, (void **)&pISciCalc);
			if (FAILED(hr))
			{
				MessageBox(hDlg, TEXT("COM Error : Cannot create instance SciCalc"), TEXT("Error"), MB_OK);
			}
			
			//hr = pISciCalc->Factorial(5, &dResult);
			//if (FAILED(hr))
			//{
			//	MessageBox(hDlg, TEXT("COM Error : Cannot Call Addition"), TEXT("Error"), MB_OK);
			//}
			//else 
			//{
			//	StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
			//	SetDlgItemText(hDlg, EDTX_RESULT, str);
			//}
            
			return TRUE;

        case WM_COMMAND:

			// suppose we are getting number in dNum1
            switch(LOWORD(wParam))
            {

				// SIN
			case PBTN_SIN:
				dNum1 = (double)rand();
				hr = pISciCalc->Sin(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Sin"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// COS
			case PBTN_COS:
				dNum1 = (double)rand();

				hr = pISciCalc->Cos(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Cos"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// TAN
			case PBTN_TAN:
				dNum1 = (double)rand();

				hr = pISciCalc->Tan(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Tan"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// SIN INVERSE
			case PBTN_SIN_INV:
				dNum1 = (double)rand();

				hr = pISciCalc->SinInverse(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Sin Inverse"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;


				// COS INVERSE
			case PBTN_COS_INV:
				dNum1 = (double)rand();

				hr = pISciCalc->CosInverse(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Cos Inverse"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;



				// TAN INVERSE
			case PBTN_TAN_INV:
				dNum1 = (double)rand();

				hr = pISciCalc->TanInverse(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Tan Inverse"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// LOG
			case PBTN_LOG:
				dNum1 = (double)rand();

				hr = pISciCalc->Log(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call LOG"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// LOGLN
			case PBTN_LOG_LN:
				dNum1 = (double)rand();

				hr = pISciCalc->Ln(dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call LOG Inverse"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// FACTORIAL
			case PBTN_FACT:
				dNum1 = rand() % 10 ;

				hr = pISciCalc->Factorial((int)dNum1, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call Factorial"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;


				// PI
			case PBTN_PI:
				hr = pISciCalc->PI(&dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call PI"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

				// XToPowerY
			case PBTN_X_TO_Y:
				dNum1 = (int)rand() % 20;

				dNum2 = (int)rand() % 10;

				hr = pISciCalc->XToPowerY(dNum1, dNum2, &dResult);
				if (FAILED(hr))
				{
					MessageBox(hDlg, TEXT("COM Error : Cannot Call X To The Power Y"), TEXT("Error"), MB_OK);
				}
				else
				{
					StringCbPrintf(str, 255, TEXT("Result %g"),dResult);
					SetDlgItemText(hDlg, EDTX_RESULT, str);
				}
				break;

			case IDCANCEL:
                    EndDialog(hDlg, 0);
                    return TRUE;
            }

            return TRUE;

	}

	return FALSE;

}

