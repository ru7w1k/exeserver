#define UNICODE

// Headers 
#include <Windows.h>
#include <strsafe.h>
#include <TlHelp32.h>  // for process snapshot related APIs and Structures 


#include "Calc.h"
#include "Server.h"

// Global Function Declaration
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
BOOL    CALLBACK StdDlgProc(HWND, UINT, WPARAM, LPARAM);	// StdDlgProc.cpp
BOOL    CALLBACK SciDlgProc(HWND, UINT, WPARAM, LPARAM);    // SciDlgProc.cpp

HRESULT StartMyClassFactories(void);
void    StopMyClassFactories(void);

extern HWND gHwnd;
extern long glNumberOfActiveComponents;
extern long glNumberOfServerLocks;

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int iCmdShow)
{
	// variable declaration
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szClassName[] = TEXT("CalcExeServer");
	HRESULT hr;
	TCHAR szTokens[] = TEXT("-/");
	TCHAR *pszTokens;
	TCHAR lpszCmdLine[255];
	wchar_t *next_token = NULL;
	int iDontShowWindow = 0; // 0 means show the window

	// code
	hr = CoInitialize(NULL);
	if (FAILED(hr))
		return(0);

	MultiByteToWideChar(CP_ACP, 0, lpCmdLine, 255, lpszCmdLine, 255);

	pszTokens = wcstok_s(lpszCmdLine, szTokens, &next_token);

	while (pszTokens != NULL)
	{
		if (_wcsicmp(pszTokens, TEXT("Embedding")) == 0)  // i.e. COM is calling me
		{
			iDontShowWindow = 1;  // dont show window, but message loop is must!
			break;
		}

		else 
		{
			exit(0);
		}

		pszTokens = wcstok_s(NULL, szTokens, &next_token);
	}


	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(WNDCLASSEX);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szClassName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// register above class
	RegisterClassEx(&wndclass);

	CoInitialize(NULL);
	hr = StartMyClassFactories();

	if (FAILED(hr))
	{
    	MessageBox(NULL, TEXT("COM Error: Class Factories Not Started!"), TEXT("COM Error"), MB_OK);		
		exit(0);
	}

	// create window
	hwnd = CreateWindow(szClassName,
		TEXT("Calculator"),
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);

	// initialize global window handle
	gHwnd = hwnd;

	if (iDontShowWindow != 1)   // TRUE if server is not called by COM Engine
	{
		ShowWindow(hwnd, iCmdShow);
		UpdateWindow(hwnd);

		// Increment server lock
		++glNumberOfServerLocks;
	}

	if (iDontShowWindow == 1) // TRUE if server is called by COM Engine
	{
		// start class factory
		hr = StartMyClassFactories();

		if (FAILED(hr))
		{
			DestroyWindow(hwnd);
			exit(0);
		}
	}

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	if (iDontShowWindow == 1)   // Only when COM calls the server
	{
		// stop class factories
		StopMyClassFactories();
	}

	// COM library uninitialization
	CoUninitialize();

	return ((int)msg.wParam);
}

// Window Procedure
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{  

    // code
    switch(iMsg)
    {
        case WM_CREATE:
            
            if (DialogBox((HINSTANCE)GetWindowLong(hwnd, GWL_HINSTANCE), MAKEINTRESOURCE(SCICALC), hwnd, (DLGPROC)SciDlgProc) == IDOK)
            {
                
            }
            else
            {
	            //MessageBox(hwnd, TEXT("Dialog Box Creation Failed"), TEXT("ERROR"), MB_OK | MB_ICONERROR);
	            DestroyWindow(hwnd);
            }

            break;

        case WM_DESTROY:
            PostQuitMessage(0);
            break;
    }

    return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}



