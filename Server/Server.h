#define STDCALC				1			// id start from 1
#define SCICALC				101			// id start from 101

#define MYICON				1000

// std calc
#define PBTN_SWITCH			2
#define PBTN_X1				3			// operation not remember			
#define PBTN_X2				4			// operation not remember
#define PBTN_DIV			5
#define PBTN_MUL			6
#define PBTN_7				7
#define PBTN_8				8
#define PBTN_9				9
#define PBTN_SUB			10
#define PBTN_4				11
#define PBTN_5				12
#define PBTN_6				13
#define PBTN_ADD			14
#define PBTN_1				15
#define PBTN_2				16
#define PBTN_3				17
#define PBTN_NEG			18
#define PBTN_0				19
#define PBTN_PT				20
#define PBTN_EQ				21
#define PBTN_SYSMIN			22
#define PBTN_SYSMAX			23
#define EDTX_RESULT			24

// sci calc
#define PBTN_X11			102
#define PBTN_X12			103
#define PBTN_X13			104
#define PBTN_X_TO_Y			105
#define PBTN_X15			106
#define PBTN_X16			107	
#define PBTN_X17			108
#define PBTN_PI				109
#define PBTN_X19			110
#define PBTN_FACT 			111
#define PBTN_LOG_LN			112
#define PBTN_LOG 			113
#define PBTN_X23			114
#define PBTN_SIN_INV		115
#define PBTN_COS_INV		116
#define PBTN_TAN_INV		117
#define PBTN_X27			118
#define PBTN_SIN			119
#define PBTN_COS			120
#define PBTN_TAN			121

