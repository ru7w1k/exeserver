#pragma once

#include<math.h>								// for sqrt()

// core function definations
double _Addition(double dVar1, double dVar2)
{
	return(dVar1 + dVar2);
}

double _Subtraction(double dVar1, double dVar2)
{
	return(dVar1 - dVar2);
}

double _Multiplication(double dVar1, double dVar2)
{
	return(dVar1 * dVar2);
}

double _Division(double dVar1, double dVar2)
{
	return(dVar1 / dVar2);
}

double _Square(double dVar)
{
	return(dVar * dVar);
}

double _SquareRoot(double dVar)
{
	return(sqrt(dVar));
}
