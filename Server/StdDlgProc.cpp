// StdDlgProc.cpp
#define UNICODE

// Headers 
#include <Windows.h>
#include <strsafe.h>

#include "Calc.h"
#include "Server.h"

BOOL CALLBACK StdDlgProc(HWND hDlg, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	double res;
	static ISciCalc *pISciCalc = NULL;
	TCHAR str[255];
	HRESULT hr;

    switch (iMsg)
	{
        case WM_INITDIALOG:
            hr = CoCreateInstance(CLSID_Calc, NULL, CLSCTX_INPROC_SERVER, IID_IStdCalc, (void **)&pISciCalc);
			if (FAILED(hr))
			{
				MessageBox(hDlg, TEXT("COM Error : Cannot create instance SciCalc"), TEXT("Error"), MB_OK);
			}
			MessageBox(NULL, TEXT("Dude i am here !!"), TEXT("MSG"), MB_OK);
			hr = pISciCalc->Factorial(7, &res);
			if (FAILED(hr))
			{
				MessageBox(hDlg, TEXT("COM Error : Cannot Call Addition"), TEXT("Error"), MB_OK);
			}
			else {
				StringCbPrintf(str, 255, TEXT("Result %g"), res);
				SetDlgItemText(hDlg, EDTX_RESULT, str);
			}
            
			return TRUE;

        case WM_COMMAND:

            switch(LOWORD(wParam))
            {
                case IDCANCEL:
                    EndDialog(hDlg, 0);
                    return TRUE;
            }

            return TRUE;

	}

	return FALSE;

}
