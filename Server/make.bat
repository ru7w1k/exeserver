@echo off
cls
echo Cleaning Workspace...
del *.obj
del *.res
del *.exe

echo Compiling Resource...
rc.exe Server.rc

echo Compiling Source Code...
cl.exe /c /EHsc Server.cpp Calc.cpp StdDlgProc.cpp SciDlgProc.cpp

echo Linking...
link.exe Server.obj Calc.obj StdDlgProc.obj SciDlgProc.obj Server.res gdi32.lib user32.lib kernel32.lib ole32.lib oleaut32.lib /OUT:Server.exe /SUBSYSTEM:WINDOWS

